import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Type } from './entities/type.entity';
import { Repository } from 'typeorm';

@Injectable()
export class TypesService {
  constructor(
    @InjectRepository(Type) private typesRopository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typesRopository.save(createTypeDto);
  }

  findAll() {
    return this.typesRopository.find();
  }

  findOne(id: number) {
    return this.typesRopository.findOneBy({ id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typesRopository.findOneByOrFail({ id });
    await this.typesRopository.update(id, updateTypeDto);
    const updatedType = await this.typesRopository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const removedType = await this.typesRopository.findOneByOrFail({ id });
    await this.typesRopository.remove(removedType);
    return removedType;
  }
}
