import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from './entities/role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private rolesRopository: Repository<Role>,
  ) {}
  create(createRoleDto: CreateRoleDto) {
    return this.rolesRopository.save(createRoleDto);
  }

  findAll() {
    return this.rolesRopository.find();
  }

  findOne(id: number) {
    return this.rolesRopository.findOneBy({ id });
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    await this.rolesRopository.findOneByOrFail({ id });
    await this.rolesRopository.update(id, updateRoleDto);
    const updatedRole = await this.rolesRopository.findOneBy({ id });
    return updatedRole;
  }

  async remove(id: number) {
    const removedRole = await this.rolesRopository.findOneByOrFail({ id });
    await this.rolesRopository.remove(removedRole);
    return removedRole;
  }
}
