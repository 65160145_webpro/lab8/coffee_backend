import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product) private productsRopository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.productsRopository.save(createProductDto);
  }

  findAll() {
    return this.productsRopository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.productsRopository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const updateProduct = await this.productsRopository.findOneOrFail({
      where: { id },
    });
    await this.productsRopository.update(id, {
      ...updateProduct,
      ...updateProductDto,
    });
    const result = await this.productsRopository.findOne({
      where: { id },
      relations: { type: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.productsRopository.findOneOrFail({
      where: { id },
    });
    await this.productsRopository.remove(deleteProduct);

    return deleteProduct;
  }
}
