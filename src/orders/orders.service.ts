import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { OrderItem } from './entities/orderItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order) private ordersRepository: Repository<Order>,
    @InjectRepository(OrderItem)
    private orderItemsRopository: Repository<OrderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order = new Order();
    const user = await this.usersRepository.findOneBy({
      id: createOrderDto.userId,
    });
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    for (const oi of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await this.orderItemsRopository.save(orderItem);
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
    }
    return this.ordersRepository.save(order);
  }

  findAll() {
    return this.ordersRepository.find({});
  }

  findOne(id: number) {
    return this.ordersRepository.findOneOrFail({
      where: { id },
      relations: { orderItems: true },
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrders = await this.ordersRepository.findOneOrFail({
      where: { id },
    });
    await this.ordersRepository.remove(deleteOrders);

    return deleteOrders;
  }
}
